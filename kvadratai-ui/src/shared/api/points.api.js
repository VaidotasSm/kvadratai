import "whatwg-fetch";

function checkStatus(res) {
    return new Promise((resolve) => {
        if (res.status >= 200 && res.status < 300) {
            return resolve(res);
        } else {
            resolve(res.json().then(body => {
                throw new HttpStatusError(res.status, body);
            }));
        }
    });
}

function post(path, body) {
    return fetch(path, {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    })
        .then(checkStatus)
        .then(res => res.headers.get('Location'))
        .then(location => {
            const array = location.split('/');
            return Number(array[array.length - 1]);
        })
        .then(id => Object.assign({}, body, {id}))
}

function put(path, body) {
    return fetch(path, {
        method: 'put',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    })
        .then(checkStatus);
}

class HttpStatusError {
    constructor(status, body) {
        this.status = status;
        this.body = body;
    }
}

const POINT_LIST_PATH = `/api/point-lists`;
const pointListsApi = {
    getPointLists(queryString) {
        const path = queryString ? `${POINT_LIST_PATH}?${queryString}` : POINT_LIST_PATH;
        return fetch(path)
            .then(checkStatus)
            .then(res => res.json());
    },

    createPointList(pointList) {
        return post(POINT_LIST_PATH, pointList);
    },

    updatePointList(pointList) {
        if(!pointList.id && pointList.id !== 0) {
            throw new Error('No id');
        }
        return put(`${POINT_LIST_PATH}/${pointList.id}`, pointList);
    },

    deletePointList({id}) {
        if(!id && id != 0) {
            throw new Error('No id');
        }
        return fetch(`${POINT_LIST_PATH}/${id}`, {method: 'delete'})
            .then(checkStatus);
    }
};

const pointsApi = {
    getPoints(listId) {
        return fetch(`/api/point-lists/${listId}/points`)
            .then(checkStatus)
            .then(res => res.json());
    },

    createPoint(listId, point) {
        return post(`/api/point-lists/${listId}/points`, point);
    },

    updatePoint(point) {
        if(!point.id && point.id !== 0) {
            throw new Error('No id');
        }
        if(!point.listId && point.listId !== 0) {
            throw new Error('No listId');
        }

        return put(`/api/point-lists/${point.listId}/points/${point.id}`, point);
    },

    calculateSquares(listId) {
        return fetch(`/api/point-lists/${listId}/points/squares`)
            .then(checkStatus)
            .then(res => res.json());
    },

    deletePoint({listId, id}) {
        return fetch(`/api/point-lists/${listId}/points/${id}`, {method: 'delete'})
            .then(checkStatus);
    }
};

export {pointListsApi, pointsApi, HttpStatusError};
