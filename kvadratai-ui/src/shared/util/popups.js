import toastr from "toastr";
import {HttpStatusError} from "../api/points.api.js";

const Popups = {
    success(message) {
        toastr.success(message);
    },
    error(message) {
        toastr.error(message);
    },
    errorInfo(errorObject, message = null) {
        console.error(errorObject);
        if (message) {
            return toastr.error(message);
        }
        if (errorObject instanceof HttpStatusError) {
            return toastr.error('Could not complete request');
        }
        toastr.error('Sorry, something went wrong');
    }
};

export {Popups};
