import {Actions} from "../actions/actions.js";

class PointListsState {
    constructor(pointLists = [], editableList = null) {
        this.pointLists = pointLists;
        this.paging = {
            totalCount: 0,
            page: 0,
            links: {
                next: null,
                previous: null
            }
        };
        this.editableList = editableList;
        this.newPointList = {name: ''};
        this.listCreateError = {};
        this.listUpdateError = {};
    }

    copyPagingState(newState) {
        newState.paging = Object.assign({}, this.paging);
    }

    setPointLists(pointLists = []) {
        const state = new PointListsState(pointLists, this.editableList);
        this.copyPagingState(state);
        state.newPointList = this.newPointList;
        return state;
    }

    afterListsLoadSuccess({entries, _metadata, links}) {
        const state = new PointListsState(entries, this.editableList);
        this.copyPagingState(state);
        state.paging.totalCount = Number(_metadata.total_count);
        state.paging.page = Number(_metadata.page);
        state.paging.links.next = links.next;
        state.paging.links.previous = links.previous;
        return state;
    }

    afterCreateSuccess(createdPointList) {
        const state = this.setPointLists([createdPointList, ...this.pointLists]);
        this.copyPagingState(state);
        state.paging.totalCount++;
        state.newPointList = {name: ''};
        return state;
    }

    afterListDeleteSuccess(pointLists) {
        const state = new PointListsState(pointLists, this.editableList);
        this.copyPagingState(state);
        state.paging.totalCount--;
        return state;
    }

    setEditableList(editableList) {
        var state = new PointListsState(this.pointLists, editableList);
        this.copyPagingState(state);
        state.newPointList = this.newPointList;
        return state;
    }

    isInEditMode(pointList) {
        return this.editableList === pointList;
    }

    afterListCreateError({err, pointList, existingListId}) {
        const state = new PointListsState(this.pointLists, this.editableList);
        this.copyPagingState(state);
        state.listCreateError = {err, pointList, existingListId};
        state.newPointList = pointList;
        return state;
    }

    getCreateErrorsOrEmpty() {
        return this.listCreateError.err ? this.listCreateError.err.body.errors : [];
    }

    afterListUpdateError({err, pointList, existingListId}) {
        const state = new PointListsState(this.pointLists, this.editableList);
        this.copyPagingState(state);
        state.listUpdateError = {err, pointList, existingListId};
        state.newPointList = this.newPointList;
        return state;
    }

    getUpdateErrorsOrEmpty() {
        return this.listUpdateError.err ? this.listUpdateError.err.body.errors : [];
    }
}

export function pointListsReducer(state = new PointListsState(), action) {
    switch (action.type) {
        case Actions.POINT_LISTS_ACTIONS_EDIT_START:
            return state.setEditableList(action.data);
        case Actions.POINT_LISTS_ACTIONS_EDIT_STOP:
            return state.setEditableList(null);
        case Actions.POINT_LISTS_LOAD_SUCCESS:
            return state.afterListsLoadSuccess(action.data);
        case Actions.POINT_LISTS_CREATE_SUCCESS:
            return state.afterCreateSuccess(action.data);
        case Actions.POINT_LISTS_CREATE_FAILURE:
            return state.afterListCreateError(action.data);
        case Actions.POINT_LISTS_UPDATE_SUCCESS:
            return state.setPointLists(state.pointLists.map(list => list.id === action.data.id ? action.data : list));
        case Actions.POINT_LISTS_UPDATE_FAILURE:
            return state.afterListUpdateError(action.data);
        case Actions.POINT_LISTS_DELETE_SUCCESS:
            return state.afterListDeleteSuccess(state.pointLists.filter(list => list.id !== action.data.id));
        default:
            return state;
    }
}

