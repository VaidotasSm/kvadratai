import {Actions} from "../actions/actions.js";

class PointsState {
    constructor(points = []) {
        this.points = points;
        this.pointCreateError = {};
        this.pointUpdateError = {};
        this.pointUpdateInProgress = null;
    }

    setPoints(points = []) {
        return new PointsState(points);
    }

    setPointUpdateInProgress(point) {
        const newState = new PointsState(this.points);
        newState.pointUpdateInProgress = point;
        return newState
    }

    addPointCreateError(errorData) {
        const newState = new PointsState(this.points);
        newState.pointCreateError = errorData;
        return newState;
    }

    addPointUpdateError(errorData) {
        const newState = new PointsState(this.points);
        newState.pointUpdateError = errorData;
        return newState;
    }
}

export function pointsReducer(state = new PointsState(), action) {
    switch (action.type) {
        case Actions.POINT_CREATE_SUCCESS:
            return state.setPoints([action.data, ...state.points]);
        case Actions.POINT_CREATE_FAILURE:
            return state.addPointCreateError(action.data);
        case Actions.POINT_UPDATE_SUCCESS:
            return state.setPoints(state.points.map(p => p.id === action.data.id ? action.data : p));
        case Actions.POINT_UPDATE_FAILURE:
            return state.addPointUpdateError(action.data);
        case Actions.POINT_DELETE_SUCCESS:
            return state.setPoints(state.points.filter(p => p.id !== action.data.id));
        case Actions.POINT_UPDATE_IN_PROGRESS:
            return state.setPointUpdateInProgress(action.data);
        case Actions.POINTS_LOAD_SUCCESS:
            return state.setPoints(action.data);
        default:
            return state;
    }
}