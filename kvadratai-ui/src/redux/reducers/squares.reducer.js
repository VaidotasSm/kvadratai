import {Actions} from "../actions/actions.js";

export function squaresReducer(state = {squares: []}, action) {
    switch (action.type) {
        case Actions.SQUARES_CALCULATE_CLEAR:
            return Object.assign({}, state, {squares: []});
        case Actions.SQUARES_CALCULATE_SUCCESS:
            return Object.assign({}, state, {squares: action.data});
        default:
            return state;
    }
}
