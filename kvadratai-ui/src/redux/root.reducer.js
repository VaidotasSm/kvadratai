import {combineReducers} from "redux";
import {pointListsReducer} from "./reducers/point-lists.reducer.js";
import {pointsReducer} from "./reducers/points.reducer.js";
import {squaresReducer} from "./reducers/squares.reducer.js";

const rootReducer = combineReducers({
    pointListsReducer: pointListsReducer,
    pointsReducer: pointsReducer,
    squaresReducer: squaresReducer
});
export {rootReducer};
