import {Actions, PointsAction} from "./actions.js";
import {pointsApi} from "../../shared/api/points.api.js";
import {Popups} from "../../shared/util/popups.js";

export function clearCalculatedSquares() {
    return PointsAction(Actions.SQUARES_CALCULATE_CLEAR);
}

function calculateSquaresSuccess(squares) {
    return PointsAction(Actions.SQUARES_CALCULATE_SUCCESS, squares);
}
export function calculateSquares(listId) {
    return function (dispatch) {
        console.log('Calculating squares');
        pointsApi.calculateSquares(listId)
            .then(res => dispatch(calculateSquaresSuccess(res.entries)))
            .catch(err => Popups.error('Something went wrong while calculating squares'));
    };
}
