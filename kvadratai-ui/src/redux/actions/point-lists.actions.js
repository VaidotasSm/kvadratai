import {Actions, PointsAction} from "./actions.js";
import {pointListsApi} from "../../shared/api/points.api.js";
import {Popups} from "../../shared/util/popups.js";
import {PointListQuery} from "../../components/points/lists/point-lists.query.js";

export function startPointListEdit(square) {
    return PointsAction(Actions.POINT_LISTS_ACTIONS_EDIT_START, square);
}

export function stopPointListEdit(square) {
    return PointsAction(Actions.POINT_LISTS_ACTIONS_EDIT_STOP, square);
}

function loadPointListsSuccess(response) {
    return PointsAction(Actions.POINT_LISTS_LOAD_SUCCESS, response);
}
export function loadPointLists(pointListQuery = new PointListQuery()) {
    return function (dispatch) {
        pointListsApi.getPointLists(pointListQuery.toQueryString())
            .then(res => dispatch(loadPointListsSuccess(res)))
            .catch(err => Popups.errorInfo(err, 'Could not load point lists'));
    };
}

function createPointListSuccess(pointList) {
    Popups.success('New list created');
    return PointsAction(Actions.POINT_LISTS_CREATE_SUCCESS, pointList);
}
function createPointListFailure(err, pointList) {
    let existingListId = null;
    if (err.status === 409) {
        existingListId = err.body.errors[0] ? err.body.errors[0].metaData.refId : null;
        Popups.error('List already exists');
    } else {
        Popups.error('Could not create list');
    }
    return PointsAction(Actions.POINT_LISTS_CREATE_FAILURE, {err, pointList, existingListId});
}
export function createPointList(pointList) {
    return function (dispatch) {
        pointListsApi.createPointList(pointList)
            .then(list => dispatch(createPointListSuccess(list)))
            .catch(err => dispatch(createPointListFailure(err, pointList)));
    };
}

function updatePointListSuccess(pointList) {
    Popups.success('List updated');
    return PointsAction(Actions.POINT_LISTS_UPDATE_SUCCESS, pointList);
}
function updatePointListFailure(err, pointList) {
    let existingListId = null;
    if (err.status === 409) {
        existingListId = err.body.errors[0] ? err.body.errors[0].metaData.refId : null;
        Popups.error('List already exists');
    } else {
        Popups.error('Could not update list');
    }
    return PointsAction(Actions.POINT_LISTS_UPDATE_FAILURE, {err, pointList, existingListId});
}
export function updatePointList(pointList) {
    return function (dispatch) {
        pointListsApi.updatePointList(pointList)
            .then(res => dispatch(updatePointListSuccess(pointList)))
            .catch(err => dispatch(updatePointListFailure(err, pointList)));
    };
}

function deletePointListSuccess(pointList) {
    Popups.success('List deleted');
    return PointsAction(Actions.POINT_LISTS_DELETE_SUCCESS, pointList);
}
export function deletePointList(pointList) {
    return function (dispatch) {
        pointListsApi.deletePointList(pointList)
            .then(res => dispatch(deletePointListSuccess(pointList)))
            .catch(err => Popups.errorInfo(err, 'Could not delete list'));
    };
}
