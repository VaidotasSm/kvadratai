import {Actions, PointsAction} from "./actions.js";
import {pointsApi} from "../../shared/api/points.api.js";
import {Popups} from "../../shared/util/popups.js";

function loadPointsSuccess(points) {
    return PointsAction(Actions.POINTS_LOAD_SUCCESS, points);
}
export function loadPoints(listId) {
    return function (dispatch) {
        pointsApi.getPoints(listId)
            .then(res => dispatch(loadPointsSuccess(res.entries)))
            .catch(Popups.errorInfo);
    };
}

function createPointSuccess(point) {
    Popups.success('New point created');
    return PointsAction(Actions.POINT_CREATE_SUCCESS, point);
}
function createPointFailure(err, point) {
    let existingPointId = null;
    if (err.status === 409) {
        existingPointId = err.body.errors[0] ? err.body.errors[0].metaData.refId : null;
        Popups.error('Point already exists');
    } else {
        Popups.error('Could not create new point');
    }
    return PointsAction(Actions.POINT_CREATE_FAILURE, {err, point, existingPointId});
}
export function createPoint(listId, point) {
    return function (dispatch) {
        pointsApi.createPoint(listId, point)
            .then(point => dispatch(createPointSuccess(point)))
            .catch(err => dispatch(createPointFailure(err, point)))
    };
}

function updatePointSuccess(point) {
    Popups.success('Point updated');
    return PointsAction(Actions.POINT_UPDATE_SUCCESS, point);
}
function updatePointFailure(err, point) {
    let existingPointId = null;
    if (err.status === 409) {
        existingPointId = err.body.errors[0] ? err.body.errors[0].metaData.refId : null;
        Popups.error('Point already exists');
    } else {
        Popups.error('Could not update point');
    }
    return PointsAction(Actions.POINT_UPDATE_FAILURE, {err, point, existingPointId});
}
export function updatePoint(point) {
    return function (dispatch) {
        pointsApi.updatePoint(point)
            .then(res => dispatch(updatePointSuccess(point)))
            .catch(err => dispatch(updatePointFailure(err, point)))
    };
}
export function pointUpdateInProgress(point) {
    return PointsAction(Actions.POINT_UPDATE_IN_PROGRESS, point);
}

function deletePointSuccess(point) {
    Popups.success('Point deleted');
    return PointsAction(Actions.POINT_DELETE_SUCCESS, point);
}
export function deletePoint(point) {
    return function (dispatch) {
        pointsApi.deletePoint(point)
            .then(res => dispatch(deletePointSuccess(point)))
            .catch(err => Popups.error('Could not delete point'))
    };
}
