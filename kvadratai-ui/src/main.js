import React from "react";
import ReactDom from "react-dom";
import {Router, Route, IndexRoute, hashHistory} from "react-router";
import {Provider} from "react-redux";
import {configureStore} from "./redux/store.js";
import {ParentPage} from "./components/generic/parent.page.comp.js";
import {NotFoundPage} from "./components/generic/not-found.page.comp";
import {PointListsPage} from "./components/points/lists/point-lists.page.comp.js";
import {PointListViewPage} from "./components/points/points/point-list-view.page.comp.js";   // custom

const store = configureStore();  // Can pass initial state
ReactDom.render(
    <Provider store={store}>
        <Router history={hashHistory}>
            <Route path="/" component={ParentPage}>
                <IndexRoute component={PointListsPage}/>
                <Route path="/points/:listId" component={PointListViewPage}/>
                <Route path="*" component={NotFoundPage}/>
            </Route>
        </Router>
    </Provider>,
    document.getElementById('app')
);
