import React from "react";
import {NavigationMenu} from "./elements/navigation-menu.comp.js";

class ParentPage extends React.Component {
    render() {
        return (
            <div>
                <NavigationMenu />
                <div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export {ParentPage};
