import React from "react";
import {Link} from "react-router";

function NavigationMenu(props) {
    return (
        <nav className="navbar navbar-default">
            <div className="container-fluid">
                <ul className="nav navbar-nav navbar-left">
                    <li>
                        <Link to="/">Point Lists</Link>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export {NavigationMenu};
