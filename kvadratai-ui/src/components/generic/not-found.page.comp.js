import React from "react";

function NotFoundPage(props) {
    return (
        <div>
            <h3>Page Not Found</h3>
        </div>
    );
}

export {NotFoundPage};
