import React from "react";
import {connect} from "react-redux";
import {loadPointLists, createPointList} from "../../../redux/actions/point-lists.actions.js";
import {PointListsTable} from "./point-lists-table.comp.js";
import {PointListSearchInput} from "./point-lists-search.comp.js";
import {PointListEdit} from "./point-list-edit.comp.js";
import {PointListsPagination} from "./point-lists-pagination.comp.js";
import {PointListQuery} from "./point-lists.query.js";

class PointListsPage extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {query: new PointListQuery()};

        this.props.dispatch(loadPointLists(this.state.query));
        this.onSaveNewList = this.onSaveNewList.bind(this);
        this.onSearchChanged = this.onSearchChanged.bind(this);
        this.onNextPageClick = this.onNextPageClick.bind(this);
        this.onPreviousPageClick = this.onPreviousPageClick.bind(this);
        this.onSortByName = this.onSortByName.bind(this);
    }

    onSaveNewList(newList) {
        this.props.dispatch(createPointList(newList));
    }

    onSearchChanged({name}) {
        this.setState({query: this.state.query.withFilter(name)});
        this.props.dispatch(loadPointLists(this.state.query));
    }

    onNextPageClick() {
        this.setState({query: this.state.query.withNextPage()});
        this.props.dispatch(loadPointLists(this.state.query));
    }

    onPreviousPageClick() {
        this.setState({query: this.state.query.withPreviousPage()});
        this.props.dispatch(loadPointLists(this.state.query));
    }

    onSortByName(order = 'asc') {
        this.setState({query: this.state.query.withSort(order)});
        this.props.dispatch(loadPointLists(this.state.query));
    }

    render() {
        const spacingStyle = {marginTop: '5px'};
        const rowColumnClass = 'col-sm-10 col-sm-offset-1 col-xs-12';
        return (
            <div>
                <div className="row">
                    <div className={rowColumnClass}>
                        <h3>Point Lists</h3>
                        <hr/>
                    </div>
                </div>
                <div className="row">
                    <div className={rowColumnClass}>
                        <div className="row">
                            <div className="col-xs-8">
                                <PointListEdit pointList={this.props.reducer.newPointList}
                                               onSaveAction={this.onSaveNewList}
                                               saveButtonName="Add New"
                                               errors={this.props.reducer.getCreateErrorsOrEmpty()}/>
                            </div>
                            <div className="col-xs-4">
                                <PointListSearchInput onSearchChanged={this.onSearchChanged}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className={rowColumnClass}>
                        <hr/>
                        <PointListsPagination page={this.props.reducer.paging.page}
                                              totalCount={this.props.reducer.paging.totalCount}
                                              linkNext={this.props.reducer.paging.links.next}
                                              linkPrevious={this.props.reducer.paging.links.previous}
                                              onNextClick={this.onNextPageClick}
                                              onPreviousClick={this.onPreviousPageClick}/>
                        <div style={spacingStyle}>
                            <PointListsTable pointLists={this.props.reducer.pointLists} onSortByName={this.onSortByName}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {reducer: state.pointListsReducer};
}
const PointListsPageWithConnect = connect(mapStateToProps)(PointListsPage);
export {PointListsPageWithConnect as PointListsPage};
