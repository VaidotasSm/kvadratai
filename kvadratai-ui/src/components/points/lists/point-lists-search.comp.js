import React from "react";

class PointListSearchInput extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {nameSearch: ''};
        this.onNameSearchChange = this.onNameSearchChange.bind(this);
    }

    onNameSearchChange(event) {
        const value = event.target.value;
        this.setState({nameSearch: value});

        if (value && value.length >= 2) {
            this.props.onSearchChanged({name: value});
        } else if (!value) {
            this.props.onSearchChanged({name: value});
        }
    }

    render() {
        return (
            <div className="form-inline">
                <div className="form-group">
                    <input type="text" className="form-control"
                           value={this.state.nameSearch} placeholder="Search by name"
                           onChange={this.onNameSearchChange}/>
                </div>
            </div>
        );
    }
}
PointListSearchInput.propTypes = {
    onSearchChanged: React.PropTypes.func.isRequired
};
export {PointListSearchInput};
