import React from "react";
import {connect} from "react-redux";
import {
    startPointListEdit,
    stopPointListEdit,
    updatePointList,
    deletePointList
} from "../../../redux/actions/point-lists.actions.js";
import {Link} from "react-router";
import {PointListEdit} from "./point-list-edit.comp.js";
import {CommonStyles} from "../../shared/common-styles.js";

class PointListPreview extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.onUpdate = this.onUpdate.bind(this);
        this.onDeleteClick = this.onDeleteClick.bind(this);
        this.onEditClick = this.onEditClick.bind(this);
        this.onEditCancelClick = this.onEditCancelClick.bind(this);
    }

    onEditClick() {
        this.props.dispatch(startPointListEdit(this.props.pointList));
    }

    onUpdate(pointList) {
        this.props.dispatch(updatePointList(pointList));
    }

    onEditCancelClick() {
        this.props.dispatch(stopPointListEdit(this.props.pointList));
    }

    onDeleteClick() {
        this.props.dispatch(deletePointList(this.props.pointList));
    }

    isListInConflict() {
        return this.props.reducer.listUpdateError.existingListId === this.props.pointList.id ||
            this.props.reducer.listCreateError.existingListId === this.props.pointList.id;
    }

    render() {
        const pointList = this.props.pointList;
        let view;
        if (this.props.reducer.isInEditMode(pointList)) {
            const errorClass = this.props.reducer.listUpdateError.err ? 'has-error' : '';
            view = (
                <div className="row">
                    <div className="col-xs-10">
                        <div className={errorClass}>
                            <PointListEdit pointList={pointList}
                                           onSaveAction={this.onUpdate}
                                           errors={this.props.reducer.getUpdateErrorsOrEmpty()}/>
                        </div>
                    </div>
                    <div className="col-xs-2">
                        <button className="btn btn-default" onClick={this.onEditCancelClick}>Cancel</button>
                    </div>
                </div>
            );
        } else {
            const errorStyle = this.isListInConflict() ? CommonStyles.errorBorder : {};
            view = (
                <div className="row" style={errorStyle}>
                    <div className="col-xs-6">
                        <Link to={`/points/${pointList.id}`}>{pointList.name}</Link>
                    </div>
                    <div className="col-xs-6">
                        <button className="btn btn-default" onClick={this.onEditClick}>Edit</button>
                        <button className="btn btn-default" onClick={this.onDeleteClick}>Delete</button>
                    </div>
                </div>
            );
        }

        return (
            view
        );
    }
}
PointListPreview.propTypes = {
    pointList: React.PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {reducer: state.pointListsReducer};
}
const PointListPreviewWithConnect = connect(mapStateToProps)(PointListPreview);
export {PointListPreviewWithConnect as PointListPreview};
