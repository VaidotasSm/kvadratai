import React from "react";

function PointListsPagination({page, totalCount, linkNext, linkPrevious, onNextClick, onPreviousClick}) {
    return (
        <div className="row">
            <div className="col-xs-6 text-center">
                <label className="label-control">Total: {totalCount}</label>
            </div>
            <div className="col-xs-6">
                <div className="form-inline">
                    <div className="form-group">
                        <button className="btn btn-sm form-control" onClick={onPreviousClick}
                                disabled={!linkPrevious}>Previous
                        </button>
                        <label className="label-control">Page: {page + 1}</label>
                        <button className="btn btn-sm  form-control" onClick={onNextClick}
                                disabled={!linkNext}>
                            Next
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}
PointListsPagination.propTypes = {
    page: React.PropTypes.number.isRequired,
    totalCount: React.PropTypes.number.isRequired,
    linkNext: React.PropTypes.string,
    linkPrevious: React.PropTypes.string,
    onNextClick: React.PropTypes.func.isRequired,
    onPreviousClick: React.PropTypes.func.isRequired,
};

export {PointListsPagination};
