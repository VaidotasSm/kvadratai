class PointListQuery {
    constructor(page = 0, pageSize = 10) {
        this.page = page;
        this.pageSize = pageSize;
        this.nameFilter = null;
        this.nameSortOrder = null;
    }

    withFilter(name) {
        this.nameFilter = name;
        return this;
    }

    withSort(order = 'asc') {
        this.nameSortOrder = order;
        return this;
    }

    withNextPage() {
        this.page++;
        return this;
    }

    withPreviousPage() {
        if (this.page > 0) {
            this.page--;
        }
        return this;
    }

    toQueryString() {
        const paging = `page=${this.page}&page_size=${this.pageSize}`;
        const nameFilter = this.nameFilter ? `&name=${this.nameFilter}` : '';
        const sort = this.nameSortOrder ? `&sort=name&sort_order=${this.nameSortOrder}` : '';
        return `${paging}${nameFilter}${sort}`;
    }
}

export {PointListQuery};
