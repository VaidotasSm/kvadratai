import React from "react";

class PointListEdit extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {pointList: this.props.pointList, errors: this.props.errors};
        this.onNameChange = this.onNameChange.bind(this);
        this.fieldHasError = this.fieldHasError.bind(this);
    }

    componentWillReceiveProps(props) {
        this.setState({pointList: props.pointList, errors: props.errors});
    }

    onNameChange(event) {
        const newState = Object.assign(
            {},
            this.state,
            {pointList: Object.assign({}, this.state.pointList, {name: event.target.value})}
        );
        this.setState(newState);
    }

    fieldHasError(fieldName) {
        return !!this.state.errors.find(err => err.field === fieldName);
    }

    render() {
        return (
            <div>
                <div className="form-inline">
                    <div className={`form-group ${this.fieldHasError('name') ? 'has-error' : ''}`}>
                        <input type="text" className="form-control"
                               value={this.state.pointList.name} onChange={this.onNameChange}/>
                    </div>
                    <div className="form-group">
                        <button className="form-control btn btn-default"
                                onClick={() => this.props.onSaveAction(this.state.pointList)}>
                            {this.props.saveButtonName ? this.props.saveButtonName : 'Save'}
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

PointListEdit.propTypes = {
    pointList: React.PropTypes.object.isRequired,
    onSaveAction: React.PropTypes.func.isRequired,
    saveButtonName: React.PropTypes.string,
    errors: React.PropTypes.array.isRequired
};
export {PointListEdit};
