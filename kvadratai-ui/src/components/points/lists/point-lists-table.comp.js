import React from "react";
import {PointListPreview} from "./point-list-preview.comp.js";

class PointListsTable extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {sortByName: 'asc'};

        this.onSortByNameClick = this.onSortByNameClick.bind(this);
    }

    onSortByNameClick(event) {
        event.preventDefault();
        const sortByName = this.state.sortByName === 'asc' ? 'desc' : 'asc';
        this.setState({sortByName});
        this.props.onSortByName(sortByName);
    }

    render() {
        return (
            <div>
                <div className="list-group">
                    <div className="list-group-item">
                        <div className="row">
                            <div className="col-xs-6">
                                <a href="#" onClick={this.onSortByNameClick}>Name</a>
                            </div>
                        </div>
                    </div>
                    {this.props.pointLists.map(list =>
                        <div key={list.id} className="list-group-item">
                            <PointListPreview pointList={list}/>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
PointListsTable.propTypes = {
    pointLists: React.PropTypes.array.isRequired,
    onSortByName: React.PropTypes.func.isRequired
};
export {PointListsTable};
