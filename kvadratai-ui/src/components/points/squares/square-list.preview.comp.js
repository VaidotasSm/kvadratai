import React from "react";

function SquareListPreview({squares}) {
    return (
        <div>
            <div className="list-group">
                <div className="list-group-item">
                    <div className="row">
                        <div className="col-xs-12">{`${squares.length} squares found.`}</div>
                    </div>
                </div>
            </div>
            <div className="list-group">
                <div className="list-group-item">
                    <div className="row">
                        <div className="col-xs-3"><strong>Square 1 (x, y)</strong></div>
                        <div className="col-xs-3"><strong>Square 2 (x, y)</strong></div>
                        <div className="col-xs-3"><strong>Square 3 (x, y)</strong></div>
                        <div className="col-xs-3"><strong>Square 4 (x, y)</strong></div>
                    </div>
                </div>
                {squares.map((square, i) =>
                    <div key={`${i}-${square.p1.id}`} className="list-group-item">
                        <div className="row">
                            <div className="col-xs-3">{square.p1.x}, {square.p1.y}</div>
                            <div className="col-xs-3">{square.p2.x}, {square.p2.y}</div>
                            <div className="col-xs-3">{square.p3.x}, {square.p3.y}</div>
                            <div className="col-xs-3">{square.p4.x}, {square.p4.y}</div>
                        </div>
                    </div>
                ) }
            </div>
        </div>
    );
}

SquareListPreview.propTypes = {
    squares: React.PropTypes.array.isRequired
};

export {SquareListPreview};
