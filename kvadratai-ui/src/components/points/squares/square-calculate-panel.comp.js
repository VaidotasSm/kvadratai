import React from "react";
import {connect} from "react-redux";
import {clearCalculatedSquares, calculateSquares} from "../../../redux/actions/squares.actions.js";
import {SquareListPreview} from "./square-list.preview.comp.js";

class SquareCalculatePanel extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {listId: props.listId};
        this.props.dispatch(clearCalculatedSquares());

        this.findSquares = this.findSquares.bind(this);
    }

    findSquares() {
        this.props.dispatch(calculateSquares(this.state.listId));
    }

    render() {
        return (
            <div>
                <button className="btn btn-default" onClick={this.findSquares}>Find Squares</button>
                <hr/>
                <SquareListPreview squares={this.props.reducer.squares}/>
            </div>
        );
    }
}
SquareCalculatePanel.propTypes = {
    listId: React.PropTypes.number.isRequired,
};

function mapStateToProps(state) {
    return {reducer: state.squaresReducer};
}
const SquareCalculatePanelWithConnect = connect(mapStateToProps)(SquareCalculatePanel);
export {SquareCalculatePanelWithConnect as SquareCalculatePanel};
