import React from "react";
import {PointPreview} from "./point-preview.comp.js";

function PointsTable({points}) {
    const columnClass = 'col-xs-4';
    return (
        <div className="list-group">
            <div className="list-group-item">
                <div className="row">
                    <div className={columnClass}><strong>X</strong></div>
                    <div className={columnClass}><strong>Y</strong></div>
                    <div className={columnClass}></div>
                </div>
            </div>
            {points.map(point =>
                <div key={point.id} className="list-group-item">
                    <PointPreview point={point} />
                </div>
            ) }
        </div>
    );
}


PointsTable.propTypes = {
    points: React.PropTypes.array.isRequired
};
export {PointsTable};
