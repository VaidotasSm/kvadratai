import React from "react";
import {connect} from "react-redux";
import {updatePoint, pointUpdateInProgress, deletePoint} from "../../../redux/actions/points.actions.js";
import {PointEdit} from "./point-edit.comp.js";
import {CommonStyles} from "../../shared/common-styles.js";


class PointPreview extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {editMode: false};

        this.onEditClick = this.onEditClick.bind(this);
        this.onPointDeleteClick = this.onPointDeleteClick.bind(this);
        this.onEditCancelClick = this.onEditCancelClick.bind(this);
        this.onPointUpdate = this.onPointUpdate.bind(this);
    }

    onEditClick() {
        this.setState({editMode: true});
    }

    onEditCancelClick() {
        this.setState({editMode: false});
    }

    isEditInProgress(point) {
        return !!(this.props.reducer.pointUpdateInProgress && this.props.reducer.pointUpdateInProgress.id === point.id);
    }

    componentWillReceiveProps(props) {
        if (!this.isEditInProgress(props.point)) {
            this.setState({editMode: false});
        }
    }

    onPointUpdate(updatedPoint) {
        this.props.dispatch(pointUpdateInProgress(updatedPoint));
        this.props.dispatch(updatePoint(updatedPoint));
    }

    onPointDeleteClick() {
        this.props.dispatch(deletePoint(this.props.point));
    }

    isCurrentPointConflicting() {
        return this.props.reducer.pointCreateError.existingPointId === this.props.point.id ||
            this.props.reducer.pointUpdateError.existingPointId === this.props.point.id;
    }

    render() {
        const columnClass = 'col-xs-4';
        let content;
        if (this.state.editMode) {
            content =
                <div className="row">
                    <div className="col-xs-8">
                        <PointEdit saveAction={this.onPointUpdate} point={this.props.point}/>
                    </div>
                    <div className="col-xs-4">
                        <button className="btn btn-default" onClick={this.onEditCancelClick}>Cancel</button>
                    </div>
                </div>;
        } else {
            const conflictStyle = this.isCurrentPointConflicting() ? CommonStyles.errorBorder : {};
            content =
                <div className="row" style={conflictStyle}>
                    <div className={columnClass}>{this.props.point.x}</div>
                    <div className={columnClass}>{this.props.point.y}</div>
                    <div className={columnClass}>
                        <button className="btn btn-default" onClick={this.onEditClick}>Edit</button>
                        <button className="btn btn-default" onClick={this.onPointDeleteClick}>Delete</button>
                    </div>
                </div>;
        }

        return (
            content
        );
    }
}

PointPreview.propTypes = {
    point: React.PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {reducer: state.pointsReducer};
}
const PointPreviewWithConnect = connect(mapStateToProps)(PointPreview);
export {PointPreviewWithConnect as PointPreview};

