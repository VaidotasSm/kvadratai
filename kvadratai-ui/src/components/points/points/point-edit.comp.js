import React from "react";
import {POINT_COORDINATES_MIN, POINT_COORDINATES_MAX} from "../shared/point.utils.js";


class PointEdit extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {point: props.point, errors: []};

        this.onFieldChanges = this.onFieldChanges.bind(this);
        this.onSave = this.onSave.bind(this);
        this.fieldHasError = this.fieldHasError.bind(this);
    }

    overrideState(point, errors = []) {
        this.setState({point, errors});
    }

    componentWillReceiveProps(props) {
        let errors = [];
        if (props.err && props.err.body) {
            errors = props.err.body.errors;
        }
        this.overrideState(props.point, errors);
    }

    fieldHasError(fieldName) {
        return !!this.state.errors.find(err => err.field === fieldName);
    }

    onFieldChanges(event) {
        const newPoint = Object.assign({}, this.state.point, {[event.target.name]: event.target.value});
        this.overrideState(newPoint);
    }

    onSave(event) {
        this.props.saveAction(this.state.point);
    }

    render() {
        return (
            <div >
                <div className="form-inline">
                    <div className={`form-group ${this.fieldHasError('x') ? 'has-error' : ''}`}>
                        <input className="form-control" type="number"
                               min={POINT_COORDINATES_MIN}
                               max={POINT_COORDINATES_MAX} placeholder="X"
                               name="x"
                               onChange={this.onFieldChanges} value={this.state.point.x}/>
                    </div>
                    <div className={`form-group ${this.fieldHasError('y') ? 'has-error' : ''}`}>
                        <input className="form-control" type="number"
                               min={POINT_COORDINATES_MIN}
                               max={POINT_COORDINATES_MAX} placeholder="Y"
                               name="y"
                               onChange={this.onFieldChanges} value={this.state.point.y}/>
                    </div>
                    <div className="form-group">
                        <button className="form-control btn btn-default"
                                onClick={() => this.props.saveAction(this.state.point)}>Save
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}
PointEdit.propTypes = {
    saveAction: React.PropTypes.func.isRequired,
    point: React.PropTypes.object.isRequired,
    err: React.PropTypes.object
};

export {PointEdit};
