import React from "react";
import {connect} from "react-redux";
import {createPoint, loadPoints} from "../../../redux/actions/points.actions.js";
import {PointsTable} from "./points-table.comp.js";
import {SquareCalculatePanel} from "../squares/square-calculate-panel.comp.js";
import {PointEdit} from "./point-edit.comp.js";

const availableTabs = {
    POINTS_MANAGE: 'POINTS_MANAGE',
    SQUARES_CALCULATE: 'SQUARES_CALCULATE'
};

class PointListViewPage extends React.Component {

    constructor(props, context) {
        super(props, context);
        const listId = Number(props.params.listId);
        this.state = {listId: listId, newPoint: {listId, x: '', y: ''}, selectedTab: availableTabs.POINTS_MANAGE};
        this.props.dispatch(loadPoints(listId));

        this.onAddNewPoint = this.onAddNewPoint.bind(this);
        this.onTabPointsSelected = this.onTabPointsSelected.bind(this);
        this.onTabSquaresSelected = this.onTabSquaresSelected.bind(this);
    }

    getNewState(selectedTab = this.state.selectedTab, newPoint = this.state.newPoint) {
        return {listId: this.state.listId, newPoint, selectedTab};
    }

    onAddNewPoint(newPoint) {
        this.setState(this.getNewState(this.state.selectedTab, {listId: this.state.listId, x: '', y: ''}));
        this.props.dispatch(createPoint(this.state.listId, newPoint));
    }

    onTabSquaresSelected(event) {
        event.preventDefault();
        this.setState(this.getNewState(availableTabs.SQUARES_CALCULATE));
    }

    onTabPointsSelected(event) {
        event.preventDefault();
        this.setState(this.getNewState(availableTabs.POINTS_MANAGE));
    }

    isTabActive(tabName) {
        return tabName === this.state.selectedTab;
    }

    render() {
        const rowColumnClass = 'col-sm-10 col-sm-offset-1 col-xs-12';

        const pointsManage = (
            <div>
                <div className="row">
                    <div className={rowColumnClass}>
                        <hr/>
                        <label>Add new</label>
                        <PointEdit saveAction={this.onAddNewPoint}
                                   point={this.props.reducer.pointCreateError.point ? this.props.reducer.pointCreateError.point : this.state.newPoint}
                                   err={this.props.reducer.pointCreateError.err}/>
                    </div>
                </div>
                <div className="row">
                    <div className={rowColumnClass}>
                        <hr/>
                        <PointsTable points={this.props.reducer.points} />
                    </div>
                </div>
            </div>
        );

        const squareCalculate = (
            <div className="row">
                <div className={rowColumnClass}>
                    <hr/>
                    <SquareCalculatePanel listId={this.state.listId}/>
                </div>
            </div>
        );

        return (
            <div>
                <div className="row">
                    <div className={rowColumnClass}>
                        <ul className="nav nav-tabs">
                            <li className={this.isTabActive(availableTabs.POINTS_MANAGE) ? 'active' : ''}>
                                <a href="#" onClick={this.onTabPointsSelected}>Manage Points</a>
                            </li>
                            <li className={this.isTabActive(availableTabs.SQUARES_CALCULATE) ? 'active' : ''}>
                                <a href="#" onClick={this.onTabSquaresSelected}>Calculate Squares</a>
                            </li>
                        </ul>
                    </div>
                </div>
                {this.isTabActive(availableTabs.POINTS_MANAGE) ? pointsManage : squareCalculate}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {reducer: state.pointsReducer};
}
const PointListViewPageWithConnect = connect(mapStateToProps)(PointListViewPage);
export {PointListViewPageWithConnect as PointListViewPage};
