
# Requirments

* Node v6.0+


# How To

Prod:

1. `npm run init` - install node and jspm packages (first time only).
2. `npm run build` - build project.
3. `npm start` - start project.
4. Open [http://localhost:8080](http://localhost:8080).

Dev:

1. `npm run init` - install node and jspm packages (first time only).
2. `npm run start:dev` - start project.
3. Open [http://localhost:8080](http://localhost:8080).
