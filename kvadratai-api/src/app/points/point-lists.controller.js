const httpUtils = require('../http/http.utils.js');
const errors = require('../http/errors');
const PointListFilter = require('../../persistence/persistence-api').PointListFilter;
const PointListValidator = require('./point-lists.validator').PointListValidator;


class PointListsController {
    constructor(persistence, pointListValidator) {
        this.persistence = persistence;
        this.pointListValidator = pointListValidator || new PointListValidator(persistence);
    }

    getAll(req, res) {
        const sortByName = req.query.sort === 'name' && req.query.sort_order ? req.query.sort_order : null;
        const filter = new PointListFilter(req.query.name, sortByName);
        if(req.query.page && req.query.page_size) {
            filter.withPaging(Number(req.query.page), Number(req.query.page_size));
        }
        this.persistence.getPointListsWithTotalCount(filter)
            .then(({entries, totalCount}) => res.status(200).json({
                entries,
                _metadata: {
                    total_count: totalCount,
                    page: filter.page
                },
                links: httpUtils.getHateoasLinks(req.originalUrl, filter.page, filter.pageSize, totalCount)
            }))
            .catch(err => errors.handleServerError(err, res));
    }

    getOne(req, res) {
        this.persistence.getPointList(req.params.id)
            .then(data => {
                if (data.length === 0 || data.length > 1) {
                    return res.sendStatus(404);
                }
                res.status(200).json(data[0]);
            })
            .catch(err => errors.handleServerError(err, res));
    }

    post(req, res) {
        this.pointListValidator.validatePointListCreationRequest(req)
            .then(validationErrors => {
                if (validationErrors.containsErrors()) {
                    throw validationErrors;
                }
                return validationErrors;
            })
            .then(() => this.persistence.createPointList(req.body))
            .then(listId => {
                res.location(httpUtils.getLocation(listId, req));
                res.sendStatus(201);
            })
            .catch(err => errors.handleServerError(err, res));
    }

    put(req, res) {
        this.pointListValidator.validatePointListCreationRequest(req)
            .then(validationErrors => {
                if (validationErrors.containsErrors()) {
                    throw validationErrors;
                }
                return validationErrors;
            })
            .then(() => this.persistence.updatePointList(req.params.id, req.body))
            .then(() => res.sendStatus(200))
            .catch(err => errors.handleServerError(err, res));
    }

    deleteOne(req, res) {
        this.persistence.deletePointList(req.params.id)
            .then(() => res.sendStatus(200))
            .catch(err => errors.handleServerError(err, res));
    }
}

module.exports.PointListsController = PointListsController;
