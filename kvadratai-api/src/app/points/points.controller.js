const httpUtils = require('../http/http.utils.js');
const errors = require('../http/errors');
const squareCalculator = require('./square.calculator');
const PointsValidator = require('./points.validator').PointsValidator;

class PointsController {
    constructor(persistence) {
        this.persistence = persistence;
        this.pointsValidator = new PointsValidator(persistence);
    }

    getAll(req, res) {
        this.persistence.getPoints(req.params.listId)
            .then(data => res.status(200).json({entries: data}))
            .catch(err => errors.handleServerError(err, res));
    }

    getOne(req, res) {
        this.persistence.getPoint(req.params.listId, req.params.id)
            .then(point => {
                if (!point) {
                    return res.sendStatus(404);
                }
                res.status(200).json(point);
            })
            .catch(err => errors.handleServerError(err, res));
    }

    post(req, res) {
        const point = Object.assign({}, req.body, {listId: Number(req.params.listId)});
        this.pointsValidator.validatePointCreation(point)
            .then(validationErrors => {
                if (validationErrors.containsErrors()) {
                    throw validationErrors;
                }
            })
            .then(() => this.persistence.createPoint(point.listId, point.x, point.y))
            .then(listId => {
                res.location(httpUtils.getLocation(listId, req));
                res.sendStatus(201);
            })
            .catch(err => errors.handleServerError(err, res));
    }

    put(req, res) {
        const point = Object.assign({}, req.body, {id: Number(req.params.id), listId: Number(req.params.listId)});
        this.pointsValidator.validatePointCreation(point)
            .then(validationErrors => {
                if (validationErrors.containsErrors()) {
                    throw validationErrors;
                }
            })
            .then(() => this.persistence.updatePoint(point))
            .then(() => res.sendStatus(200))
            .catch(err => errors.handleServerError(err, res));
    }

    deleteOne(req, res) {
        this.persistence.deletePoint(req.params.listId, req.params.id)
            .then(() => res.sendStatus(200))
            .catch(err => errors.handleServerError(err, res));
    }

    getListSquares(req, res) {
        this.persistence.getPoints(req.params.listId)
            .then(points => {
                const squares = squareCalculator.calculateSquare(points);
                res.status(200).json({entries: squares});
            })
            .catch(err => errors.handleServerError(err, res));
    }

}

module.exports.PointsController = PointsController;
