const errors = require('../http/errors');
const PointListFilter = require('../../persistence/persistence-api').PointListFilter;

class PointListValidator {
    constructor(persistence) {
        this.persistence = persistence;
    }

    validatePointListCreationRequest(req) {
        return new Promise(resolve => resolve(new errors.ValidationErrors()))
            .then(validationErrors => {
                if (!req.body) {
                    return validationErrors.addError('body', 'Cannot be empty');
                }
                if (!req.body.name) {
                    return validationErrors.addError('name', 'Cannot be empty');
                }
                return validationErrors;
            })
            .then(validationErrors => {
                if (validationErrors.containsErrors()) {
                    return validationErrors;
                }
                return this.persistence.getPointLists(new PointListFilter().withExactName(req.body.name))
                    .then(existingLists => {
                        if (existingLists.length > 0) {
                            validationErrors
                                .addError('name', 'Point list with same name already exists', {refId: existingLists[0].id})
                                .setStatus(409);
                        }
                        return validationErrors;
                    });
            });
    }
}

module.exports.PointListValidator = PointListValidator;
