module.exports.calculateSquare = function (points) {
    if (!points || points.length === 0) {
        return [];
    }
    const pointsMapped = points.map(p => new Point(p));

    const squares = [];
    for (let i1 = 0; i1 < pointsMapped.length; i1++) {
        if (pointsMapped.length - i1 < 4) {
            break;
        }

        var p1 = pointsMapped[i1];
        for (let i2 = i1 + 1; i2 < pointsMapped.length; i2++) {
            var p2 = pointsMapped[i2];
            for (let i3 = i2 + 1; i3 < pointsMapped.length; i3++) {
                var p3 = pointsMapped[i3];
                for (let i4 = i3 + 1; i4 < pointsMapped.length; i4++) {
                    var p4 = pointsMapped[i4];

                    if (canFormSquare(p1, p2, p3, p4)) {
                        squares.push(new Square(p1, p2, p3, p4));
                    }
                }
            }
        }
    }
    return squares;
};

function canFormSquare(p1, p2, p3, p4) {
    const set = new Set([p1, p2, p3, p4]);
    if (set.size !== 4) {
        return false;
    }

    const distances = new Map();
    addDistanceToMap(distances, p1.calculateDistance(p2));
    addDistanceToMap(distances, p1.calculateDistance(p3));
    addDistanceToMap(distances, p1.calculateDistance(p4));
    addDistanceToMap(distances, p2.calculateDistance(p3));
    addDistanceToMap(distances, p2.calculateDistance(p4));
    addDistanceToMap(distances, p3.calculateDistance(p4));
    if (distances.size !== 2) {
        return false;
    }

    const counts = [];
    distances.forEach((count) => {
        counts.push(count);
    });
    return !!counts.find(c => c === 4) && !!counts.find(c => c === 2);
}

function addDistanceToMap(map, distance) {
    let oldCount = map.get(distance);
    map.set(distance, oldCount ? oldCount + 1 : 1);
}

class Point {
    constructor({id, x, y}) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    calculateDistance(other) {
        return Math.hypot(other.x - this.x, other.y - this.y);
    }
}

class Square {
    constructor(p1, p2, p3, p4) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }
}
