const errors = require('../http/errors');
const PointsFilter = require('../../persistence/persistence-api').PointsFilter;
const MAX_COORDINATE = 5000;

class PointsValidator {
    constructor(persistence) {
        this.persistence = persistence;
    }

    validatePointCreation(point) {
        return new Promise(resolve => resolve(new errors.ValidationErrors()))
            .then(validationErrors => {
                if (!point) {
                    return validationErrors.addError('body', 'Cannot be empty');
                }
                if (!point.x && point.x !== 0) {
                    return validationErrors.addError('x', 'Cannot be empty');
                }
                if (!point.y && point.y !== 0) {
                    return validationErrors.addError('y', 'Cannot be empty');
                }
                if (Math.abs(point.x) > MAX_COORDINATE) {
                    return validationErrors.addError('x', 'Cannot be outside [-5000...5000]');
                }
                if (Math.abs(point.y) > MAX_COORDINATE) {
                    return validationErrors.addError('y', 'Cannot be outside [-5000...5000]');
                }
                return validationErrors;
            })
            .then(validationErrors => {
                if (validationErrors.containsErrors()) {
                    return validationErrors;
                }
                return this.persistence.getPointsByFilter(new PointsFilter(point.listId, point.x, point.y))
                    .then(existingPoints => {
                        if (existingPoints.length > 0) {
                            validationErrors
                                .addError('x', 'Duplicate coordinates', {refId: existingPoints[0].id})
                                .addError('y', 'Duplicate coordinates', {refId: existingPoints[0].id})
                                .setStatus(409);
                        }
                        return validationErrors;
                    });
            });
    }
}

module.exports.PointsValidator = PointsValidator;
