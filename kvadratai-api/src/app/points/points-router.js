const express = require('express');
const PointListsController = require('./point-lists.controller').PointListsController;
const PointsController = require('./points.controller').PointsController;

module.exports.pointLists = function (persistence) {
    const pointListsController = new PointListsController(persistence);
    const pointsController = new PointsController(persistence);

    const pointsRouter = express.Router()
        .get('/:listId/points/squares', pointsController.getListSquares.bind(pointsController))
        .get('/:listId/points', pointsController.getAll.bind(pointsController))
        .get('/:listId/points/:id', pointsController.getOne.bind(pointsController))
        .post('/:listId/points', pointsController.post.bind(pointsController))
        .put('/:listId/points/:id', pointsController.put.bind(pointsController))
        .delete('/:listId/points/:id', pointsController.deleteOne.bind(pointsController));
    return express.Router()
        .get('/', pointListsController.getAll.bind(pointListsController))
        .get('/:id', pointListsController.getOne.bind(pointListsController))
        .post('/', pointListsController.post.bind(pointListsController))
        .put('/:id', pointListsController.put.bind(pointListsController))
        .delete('/:id', pointListsController.deleteOne.bind(pointListsController))
        .use('/', pointsRouter);
};
