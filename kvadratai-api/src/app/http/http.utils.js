
module.exports.getLocation = function(id, req) {
    return `${req.protocol}://${req.headers.host}${req.originalUrl}/${id}`;
};

module.exports.getHateoasLinks = function(self, page, pageSize, totalCount) {
    const previous = page > 0 ? self.replace(`page=${page}`, `page=${page - 1}`) : null;
    const hasNext = (page + 1) * pageSize < totalCount;
    const next = hasNext ? self.replace(`page=${page}`, `page=${page + 1}`) : null;
    return {
        self: self,
        next,
        previous
    };
};
