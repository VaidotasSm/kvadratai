class ValidationErrors {
    constructor(error = null) {
        this.errors = [];
        this.status = 400;
        if (error) {
            this.addError(error.field, error.message, error.metaData);
        }
    }

    containsErrors() {
        return !!this.errors.length;
    }

    addError(field, message, metaData = {}) {
        this.errors.push({field, message, metaData});
        return this;
    }

    setStatus(status) {
        this.status = status;
        return this;
    }
}

function sendErrorResponse(res, validationErrors) {
    res.status(validationErrors.status).json({errors: validationErrors.errors});
}

function handleServerError(err, res) {
    if (err instanceof ValidationErrors) {
        sendErrorResponse(res, err);
    } else {
        console.error(err);
        res.status(500).json(err);
    }
}

module.exports.ValidationErrors = ValidationErrors;
module.exports.handleServerError = handleServerError;
