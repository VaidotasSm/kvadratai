const express = require('express');
const app = express();
const path = require("path");
const bodyParser = require('body-parser');
const routing = require('./routing');

module.exports = class AppServer {
    constructor(persistence, profile = 'prod', port = 8080) {
        this.profile = profile;
        this.port = port;
        this.persistence = persistence;
    }

    start() {
        setupInfrastructure();
        setupStaticResources(this.profile);

        app.use('/', routing.router(this.persistence));
        return {
            handle: app.listen(this.port, () => {
                console.log(`Started on port ${this.port}`);
            }),
            app: app
        };
    }
};

function setupInfrastructure() {
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
}

function setupStaticResources(profile) {
    console.log(`Using '${profile}' profile`);
    if (profile === 'dev') {
        app.use('/app', express.static(path.join(__dirname, '../../..', 'kvadratai-ui')));
    } else {
        app.use('/app', express.static(path.join(__dirname, '../../..', 'kvadratai-ui/dest')));
    }
}
