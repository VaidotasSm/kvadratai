const express = require('express');
const pointRouter = require('./points/points-router');

module.exports.router = function (persistence) {

    const apiRouter = express.Router()
        .use('/point-lists', pointRouter.pointLists(persistence));

    return express.Router()
        .use(function (req, res, next) {
            console.log(`${req.url} - ${req.method}`);
            next();
        })
        .use('/api', apiRouter)
        .get('/', function (req, res) {
            res.redirect('/app/index.html');
        });
};
