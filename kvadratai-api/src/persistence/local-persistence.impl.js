const alasql = require('alasql');
const PointListFilter = require('./persistence-api').PointListFilter;

function toPoint(dataEntry) {
    return {id: dataEntry.id, listId: dataEntry.list_id, x: dataEntry.x, y: dataEntry.y};
}

function pointListsWhere(filter) {
    if (filter.nameLike && filter.nameLike.length > 2) {
        return `WHERE LOWER(name) LIKE '%${filter.nameLike.toLowerCase()}%'`;
    }
    if (filter.nameExact) {
        return `WHERE LOWER(name) = '${filter.nameExact.toLowerCase()}'`;
    }
    return '';
}

class LocalPersistence {

    constructor(samplePointLists = [], samplePoints = []) {
        console.log('~~~~~~~~~~~~~~~~ Recreating Persistence ~~~~~~~~~~~~~~~~');
        alasql('DROP TABLE IF EXISTS point_lists');
        alasql('DROP TABLE IF EXISTS points');
        alasql(`
             CREATE TABLE point_lists (
                id INT PRIMARY KEY IDENTITY,
                name STRING,
                UNIQUE(name)
            );
        `);
        alasql(`
             CREATE TABLE points (
                id INT PRIMARY KEY IDENTITY,
                list_id INT REFERENCES point_lists(id),
                x INT,
                y INT,
                UNIQUE(list_id, x, y)
            );
        `);

        if (samplePointLists.length > 0) {
            samplePointLists.forEach(list => this.createPointList(list));
        }
        if (samplePoints.length > 0) {
            samplePoints.forEach(p => this.createPoint(p.listId, p.x, p.y));
        }
    }

    getPointListsWithTotalCount(filter = new PointListFilter()) {
        return this.getPointLists(filter)
            .then(entries => this.getPointListsCount(filter).then(countData => {
                return {
                    entries,
                    totalCount: countData[0].total_count
                };
            }));
    }

    getPointLists(filter = new PointListFilter()) {
        const orderBy = `ORDER BY name ${filter.sortByName}`;
        return alasql.promise(`
            SELECT * FROM point_lists ${pointListsWhere(filter)} ${orderBy} 
            LIMIT ${filter.pageSize} OFFSET ${(filter.page * filter.pageSize) + 1}
        `);
    }

    getPointListsCount(filter) {
        return alasql.promise(`SELECT COUNT(*) as total_count FROM point_lists ${pointListsWhere(filter)}`);
    }

    getPointList(id) {
        return alasql.promise(`SELECT * FROM point_lists WHERE point_lists.id = ${id}`);
    }

    createPointList(list) {
        return alasql.promise('SELECT max(id) as id FROM point_lists')
            .then(maxId => maxId[0].id ? maxId[0].id + 1 : 1)
            .then(id => alasql.promise(`INSERT INTO point_lists (id, name) values (${id}, '${list.name}')`)
                .then(res => id)
            );
    }

    updatePointList(id, list) {
        return alasql.promise(`UPDATE point_lists SET name = '${list.name}' WHERE id = ${id}`);
    }

    deletePointList(id) {
        return alasql.promise(`DELETE from point_lists WHERE id = ${id}`);
    }

    getPoints(listId) {
        return alasql.promise(`SELECT * FROM points WHERE list_id = ${listId}`)
            .then(data => data.map(entry => toPoint(entry)))
            .then(points => points);
    }

    getPointsByFilter(pointsFilter) {
        if (!pointsFilter.listId) {
            throw Error('Cannot Search without list ID.');
        }
        let where = `WHERE list_id = ${pointsFilter.listId}`;
        where += pointsFilter.x ? ` AND x = ${pointsFilter.x}` : '';
        where += pointsFilter.y ? ` AND y = ${pointsFilter.y}` : '';
        return alasql.promise(`SELECT * FROM points ${where}`)
            .then(data => data.map(entry => toPoint(entry)))
            .then(points => points);
    }

    getPoint(listId, pointId) {
        return alasql.promise(`SELECT * FROM points WHERE id = ${pointId} AND list_id = ${listId}`)
            .then(data => {
                if (data.length === 0 || data.length > 1) {
                    return null;
                }
                return toPoint(data[0]);
            });
    }

    createPoint(listId, x, y) {
        return alasql.promise('SELECT max(id) as id FROM points')
            .then(maxId => {
                return maxId[0].id ? maxId[0].id + 1 : 1;
            })
            .then(id => {
                return alasql.promise(`INSERT INTO points (id, list_id, x, y) values (${id}, ${listId}, ${x}, ${y})`)
                    .then(res => {
                        if (!res) {
                            throw new Error(`Could not create point: (${id}, ${listId}, ${x}, ${y})`);
                        }
                        return id;
                    });
            });
    }

    updatePoint({id, listId, x, y}) {
        return alasql.promise(`UPDATE points SET x = ${x}, y = ${y} WHERE id = ${id} AND list_id = ${listId}`);
    }

    deletePoint(listId, id) {
        return alasql.promise(`DELETE from points WHERE id = ${id} AND list_id = ${listId}`);
    }
}

module.exports.LocalPersistence = LocalPersistence;
