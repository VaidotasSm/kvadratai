class PointListFilter {
    constructor(nameLike, sortByName) {
        this.nameLike = nameLike;
        this.nameExact = null;
        this.sortByName = sortByName || 'asc';
        this.page = 0;
        this.pageSize = 20;
    }

    withPaging(page = 0, pageSize = 20) {
        this.page = page;
        this.pageSize = pageSize;
        return this;
    }

    withExactName(name) {
        this.nameExact = name;
        return this;
    }
}

class PointsFilter {
    constructor(listId, x, y) {
        this.listId = listId;
        this.x = x;
        this.y = y;
    }
}

module.exports.PointListFilter = PointListFilter;
module.exports.PointsFilter = PointsFilter;
