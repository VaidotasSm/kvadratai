const AppServer = require('./app/app-server');
const LocalPersistence = require('./persistence/local-persistence.impl').LocalPersistence;

let persistence;
if (process.env.PROFILE == 'dev') {
    persistence = new LocalPersistence(
        [
            {name: 'List1'},
            {name: 'List2'},
            {name: 'List3'},
            {name: 'List4'},
            {name: 'List5'}
        ],
        [
            {listId: 1, x: 1, y: 1},
            {listId: 1, x: 1, y: 3},
            {listId: 1, x: 3, y: 1},
            {listId: 1, x: 3, y: 3},
            {listId: 1, x: 0, y: 2},
            {listId: 1, x: 2, y: 0},
            {listId: 1, x: 4, y: 2},
            {listId: 1, x: 2, y: 4},
            {listId: 1, x: 0, y: -2},
            {listId: 1, x: -2, y: -0},
            {listId: 1, x: -4, y: -2},
            {listId: 1, x: -2, y: -4}
        ]);
} else {
    persistence = new LocalPersistence();
}
new AppServer(persistence, process.env.PROFILE, process.env.PORT).start();
