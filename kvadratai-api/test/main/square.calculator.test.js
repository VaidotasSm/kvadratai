const chai = require("chai");
const expect = chai.expect;
const calculator = require('../../src/app/points/square.calculator');

describe('Square calculator: ', () => {

    describe('Calculates squares: ', () => {

        it('Is empty when no squares present', () => {
            const result = calculator.calculateSquare([
                {id: 1, x: 1, y: 1},
                {id: 2, x: 1, y: 4},
                {id: 3, x: 3, y: 1},
                {id: 4, x: 3, y: 3},
                {id: 5, x: 8, y: -3},
                {id: 6, x: 8, y: -5},
                {id: 7, x: 21, y: 22},
                {id: 8, x: 18, y: -3},
            ]);
            expect(result.length).to.equal(0);
        });

        it('Finds horizontal square', () => {
            const result = calculator.calculateSquare([
                {id: 1, x: 1, y: 1},
                {id: 2, x: 1, y: 3},
                {id: 3, x: 3, y: 1},
                {id: 4, x: 3, y: 3}
            ]);
            expect(result.length).to.equal(1);
        });

        it('Finds horizontal negative square', () => {
            const result = calculator.calculateSquare([
                {id: 1, x: -1, y: -1},
                {id: 2, x: -1, y: -3},
                {id: 3, x: -3, y: -1},
                {id: 4, x: -3, y: -3}
            ]);
            expect(result.length).to.equal(1);
        });

        it('Finds diagonal square', () => {
            const result = calculator.calculateSquare([
                {id: 1, x: 0, y: 2},
                {id: 2, x: 2, y: 0},
                {id: 3, x: 4, y: 2},
                {id: 4, x: 2, y: 4}
            ]);
            expect(result.length).to.equal(1);
        });

        it('Finds diagonal negative square', () => {
            const result = calculator.calculateSquare([
                {id: 1, x: 0, y: -2},
                {id: 2, x: -2, y: -0},
                {id: 3, x: -4, y: -2},
                {id: 4, x: -2, y: -4}
            ]);
            expect(result.length).to.equal(1);
        });


        it('Finds multiple squares', () => {
            const result = calculator.calculateSquare([
                {id: 1, x: 1, y: 1},
                {id: 2, x: 1, y: 3},
                {id: 3, x: 3, y: 1},
                {id: 4, x: 3, y: 3},

                {id: 1, x: 0, y: 2},
                {id: 2, x: 2, y: 0},
                {id: 3, x: 4, y: 2},
                {id: 4, x: 2, y: 4},

                {id: 1, x: 0, y: -2},
                {id: 2, x: -2, y: -0},
                {id: 3, x: -4, y: -2},
                {id: 4, x: -2, y: -4}
            ]);
            expect(result.length).to.equal(4);  // One accidental square
        });
    });

    describe('Handles corner cases: ', () => {

        it('Is empty when no points', () => {
            const result = calculator.calculateSquare([]);
            expect(result.length).to.equal(0);
        });

        it('Is empty when null', () => {
            const result = calculator.calculateSquare(null);
            expect(result.length).to.equal(0);
        });
    });

});

