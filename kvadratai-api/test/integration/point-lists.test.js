const chai = require("chai");
const expect = chai.expect;
chai.use(require('chai-things'));

const AppServer = require('../../src/app/app-server');
const LocalPersistence = require('../../src/persistence/local-persistence.impl').LocalPersistence;
const supertest = require('supertest-as-promised');
const api = require('./helper/api.js');

describe('Point Lists: ', () => {

    // TODO Should be global for multiple files
    let serverHandle;
    let agent;
    let pointListsApi;
    before(done => {
        serverHandle = new AppServer(new LocalPersistence(), 'dev').start();
        agent = supertest.agent(serverHandle.app);
        pointListsApi = new api.PointListsApi(agent);
        done();
    });
    after(done => {
        serverHandle.handle.close();
        done();
    });

    describe('CRUD:', () => {
        it('Contains no point lists at beginning', done => {
            pointListsApi.retrieveLists()
                .expect(res => {
                    expect(res.body.entries).to.have.length(0);
                })
                .end(done);
        });

        it('Retrieves point-lists with pagination and sorting', done => {
            pointListsApi.createList('List01')
                .then(() => pointListsApi.createList('List02'))
                .then(() => pointListsApi.createList('List03'))
                .then(() => pointListsApi.createList('List04'))
                .then(() => pointListsApi.createList('List05'))
                .then(() => pointListsApi.createList('List06'))
                .then(() => pointListsApi.createList('List07'))
                .then(() => pointListsApi.createList('List08'))
                .then(() => pointListsApi.createList('List09'))
                .then(() => pointListsApi.createList('List10'))
                .then(() => pointListsApi.createList('List11'))
                .then(() => pointListsApi.createList('List12'))
                .then(() => pointListsApi.createList('List13'))
                .then(() => pointListsApi.createList('List14'))
                .then(() => pointListsApi.createList('List15'))
                .then(() => pointListsApi.retrieveLists('page=0&page_size=5&sort=name&sort_order=desc')
                    .then(res => {
                        expect(res.body.entries).to.have.length(5);
                        expect(res.body._metadata.total_count).to.equal(15);
                        expect(res.body.links.self).to.equal('/api/point-lists?page=0&page_size=5&sort=name&sort_order=desc');
                        expect(res.body.links.previous).to.not.exist;
                        expect(res.body.links.next).to.equal('/api/point-lists?page=1&page_size=5&sort=name&sort_order=desc');
                        expect(res.body.entries[0].name).to.equal('List15');
                        expect(res.body.entries[1].name).to.equal('List14');
                        expect(res.body.entries[2].name).to.equal('List13');
                        expect(res.body.entries[3].name).to.equal('List12');
                        expect(res.body.entries[4].name).to.equal('List11');
                    })
                )
                .then(() => pointListsApi.retrieveLists('page=1&page_size=5&sort=name&sort_order=desc')
                    .then(res => {
                        expect(res.body.entries).to.have.length(5);
                        expect(res.body._metadata.total_count).to.equal(15);
                        expect(res.body.links.self).to.equal('/api/point-lists?page=1&page_size=5&sort=name&sort_order=desc');
                        expect(res.body.links.previous).to.equal('/api/point-lists?page=0&page_size=5&sort=name&sort_order=desc');
                        expect(res.body.links.next).to.equal('/api/point-lists?page=2&page_size=5&sort=name&sort_order=desc');
                        expect(res.body.entries[0].name).to.equal('List10');
                        expect(res.body.entries[1].name).to.equal('List09');
                        expect(res.body.entries[2].name).to.equal('List08');
                        expect(res.body.entries[3].name).to.equal('List07');
                        expect(res.body.entries[4].name).to.equal('List06');
                    })
                )
                .then(() => pointListsApi.retrieveLists('page=2&page_size=5&sort=name&sort_order=desc')
                    .then(res => {
                        expect(res.body.entries).to.have.length(5);
                        expect(res.body._metadata.total_count).to.equal(15);
                        expect(res.body.links.self).to.equal('/api/point-lists?page=2&page_size=5&sort=name&sort_order=desc');
                        expect(res.body.links.previous).to.equal('/api/point-lists?page=1&page_size=5&sort=name&sort_order=desc');
                        expect(res.body.links.next).to.not.exist;
                        expect(res.body.entries[0].name).to.equal('List05');
                        expect(res.body.entries[1].name).to.equal('List04');
                        expect(res.body.entries[2].name).to.equal('List03');
                        expect(res.body.entries[3].name).to.equal('List02');
                        expect(res.body.entries[4].name).to.equal('List01');
                    })
                )
                .then(() => done())
                .catch(err => done(err));
        });

        it('Can filter point-lists', done => {
            pointListsApi.createList('Can filter 1 point-lists')
                .then(() => pointListsApi.createList('Can filter 2 point-lists'))
                .then(() => pointListsApi.createList('Can filter 3 point-lists'))
                .then(() => pointListsApi.retrieveLists('name=filter 2 point')
                    .then(res => {
                        expect(res.body.entries).to.have.length(1);
                    })
                )
                .then(() => done())
                .catch(err => done(err));
        });

        it('Creates and retrieves point-list', done => {
            pointListsApi.createList('List-created')
                .then(id => expectListToExist({id: id, name: 'List-created'}))
                .then(() => done())
                .catch(err => done(err));
        });

        it('Updates point-list', done => {
            pointListsApi.createList('List-update')
                .then(id => pointListsApi.updateList(id, 'List1 updated1')
                    .expect(200)
                    .then(res => id)
                )
                .then(id => expectListToExist({id: id, name: 'List1 updated1'}))
                .then(() => done())
                .catch(err => done(err));
        });

        it('Deletes point list', done => {
            pointListsApi.createList('List-deletion')
                .then(id => pointListsApi.deleteList(id))
                .then(id => pointListsApi.retrieveList(id)
                    .expect(404)
                )
                .then(() => done())
                .catch(err => done(err));
        });
    });

    describe('Validation: ', () => {
        it('Cannot create point-list with empty name', done => {
            pointListsApi.createListRaw('')
                .expect(400, (err, res) => {
                    expect(res.body.errors[0].field).to.equal('name');
                    done(err, res);
                });
        });

        it('Cannot create point-list with existing name', done => {
            pointListsApi.createList('List-with-existing-name')
                .then(() => pointListsApi.createListRaw('List-with-existing-name')
                    .expect(409)
                    .then(res => {
                        expect(res.body.errors[0].field).to.equal('name');
                        expect(res.body.errors[0].metaData.refId).to.not.be.undefined;
                    })
                )
                .then(() => done())
                .catch(err => done(err));
        });

        it('Cannot update point-list with existing name', done => {
            const existingName = 'List-with-existing-name-update';

            pointListsApi.createList(existingName)
                .then(() => pointListsApi.createList('Good Name for now'))
                .then(id => pointListsApi.updateList(id, existingName)
                    .expect(409)
                    .then(res => {
                        expect(res.body.errors[0].field).to.equal('name');
                        expect(res.body.errors[0].metaData.refId).to.not.be.undefined;
                    })
                )
                .then(() => done())
                .catch(err => done(err));
        });

        it('Cannot update point-list with empty name', done => {
            pointListsApi.createList('List-update')
                .then(id => pointListsApi.updateList(id, '')
                    .expect(400)
                    .then(res => {
                        expect(res.body.errors[0].field).to.equal('name');
                    })
                )
                .then(() => done())
                .catch(err => done(err));
        });
    });

    function expectListToExist({id, name}) {
        return pointListsApi.retrieveList(id).expect(200)
            .then(res => {
                expect(res.body.id).to.equal(id);
                expect(res.body.name).to.equal(name);
            });
    }
});

function expectToContainList(lists, name) {
    const listsWithoutId = lists.map(({id, name}) => {
        return {id: !!id, name};
    });
    expect(listsWithoutId).to.include.something.that.deep.equals({id: true, name});
}
