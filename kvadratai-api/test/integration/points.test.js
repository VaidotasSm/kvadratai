const chai = require("chai");
const expect = chai.expect;
chai.use(require('chai-things'));
const AppServer = require('../../src/app/app-server');
const LocalPersistence = require('../../src/persistence/local-persistence.impl').LocalPersistence;
const supertest = require("supertest-as-promised");
const api = require('./helper/api.js');

describe('Points: ', () => {

    // TODO Should be global for multiple files
    let serverHandle;
    let agent;
    let pointListsApi;
    let pointsApi;
    before(done => {
        serverHandle = new AppServer(new LocalPersistence(), 'dev').start();
        agent = supertest.agent(serverHandle.app);
        pointListsApi = new api.PointListsApi(agent);
        pointsApi = new api.PointsApi(agent);
        done();
    });

    after(done => {
        serverHandle.handle.close();
        done();
    });

    describe('CRUD: ', () => {

        it('New List contains no Points', done => {
            pointListsApi.createList('New List contains no Points')
                .then(listId => pointsApi.retrievePoints(listId).then(res => {
                    expect(res.body.entries).to.have.length(0);
                }))
                .then(() => done())
                .catch(err => done(err));
        });

        it('List contains Points with fields', done => {
            pointListsApi.createList('List contains Points with fields')
                .then(listId => pointsApi.createPoint(listId, {x: 5000, y: -5000}).then(() => listId))
                .then(listId => pointsApi.createPoint(listId, {x: -5000, y: 5000}).then(() => listId))
                .then(listId => pointsApi.createPoint(listId, {x: 13, y: 23}).then(() => listId))
                .then(listId => pointsApi.retrievePoints(listId).then(res => {
                    expect(res.body.entries).to.have.length(3);
                    expectToContainPoint(res.body.entries, listId, 5000, -5000);
                    expectToContainPoint(res.body.entries, listId, -5000, 5000);
                    expectToContainPoint(res.body.entries, listId, 13, 23);
                }))
                .then(() => done())
                .catch(err => done(err));
        });

        it('Calculates squares', done => {
            pointListsApi.createList('Calculates squares')
                .then(listId => pointsApi.createPoint(listId, {x: 1, y: 1}).then(() => listId))
                .then(listId => pointsApi.createPoint(listId, {x: 1, y: 3}).then(() => listId))
                .then(listId => pointsApi.createPoint(listId, {x: 3, y: 3}).then(() => listId))
                .then(listId => pointsApi.createPoint(listId, {x: 3, y: 1}).then(() => listId))
                .then(listId => pointsApi.retrieveSqueres(listId).then(res => {
                    expect(res.body.entries).to.have.length(1);
                    expectCorrectSquareStructure(res.body.entries[0]);
                }))
                .then(() => done())
                .catch(err => done(err));
        });

        it('Calculates zero squares when empty', done => {
            pointListsApi.createList('Calculates zero squares when empty')
                .then(listId => pointsApi.retrieveSqueres(listId).then(res => {
                    expect(res.body.entries).to.have.length(0);
                }))
                .then(() => done())
                .catch(err => done(err));
        });

        it('Created point exists with all fields', done => {
            pointListsApi.createList('Created point exists')
                .then(listId => pointsApi.createPoint(listId, {x: 10, y: 20}))
                .then(({listId, pointId}) => pointsApi.retrievePoint(listId, pointId)
                    .expect(200)
                    .then(res => {
                        expect(res.body.id).to.equal(pointId);
                        expect(res.body.listId).to.equal(listId);
                        expect(res.body.x).to.equal(10);
                        expect(res.body.y).to.equal(20);
                    }))
                .then(() => done())
                .catch(err => done(err));
        });

        it('Can create zero coordinate point', done => {
            pointListsApi.createList('Can create zero coordinate point')
                .then(listId => pointsApi.createPointRaw(listId, {x: 0, y: 0})
                    .expect(201)
                )
                .then(() => done())
                .catch(err => done(err));
        });

        it('Can retrieve all Points from list', done => {
            pointListsApi.createList('Can retrieve all Points from list')
                .then(listId => Promise.all([
                        pointsApi.createPoint(listId, {x: 10, y: 10}),
                        pointsApi.createPoint(listId, {x: 11, y: 11}),
                        pointsApi.createPoint(listId, {x: 12, y: 12})
                    ]).then(() => listId)
                )
                .then(listId => pointsApi.retrievePoints(listId).then(res => {
                    expect(res.body.entries).to.have.length(3);
                }))
                .then(() => done())
                .catch(err => done(err));
        });

        it('Can update point', done => {
            pointListsApi.createList('Can update point')
                .then(listId => pointsApi.createPoint(listId, {x: 10, y: 20}))
                .then(({listId, pointId}) => pointsApi.updatePoint(listId, pointId, {x: 19, y: 29})
                    .expect(200)
                    .then(() => {
                        return {listId, pointId};
                    })
                )
                .then(({listId, pointId}) => pointsApi.retrievePoint(listId, pointId)
                    .expect(200)
                    .then(res => {
                        expect(res.body.x).to.equal(19);
                        expect(res.body.y).to.equal(29);
                    })
                )
                .then(() => done())
                .catch(err => done(err));
        });

        it('Can delete point', done => {
            pointListsApi.createList('Can delete point')
                .then(listId => pointsApi.createPoint(listId, {x: 10, y: 20}))
                .then(({listId, pointId}) => pointsApi.deletePoint(listId, pointId))
                .then(({listId, pointId}) => pointsApi.retrievePoint(listId, pointId)
                    .expect(404)
                )
                .then(() => done())
                .catch(err => done(err));
        });

        it('Can delete all points in list', done => {
            done();
        });

    });

    describe('Validation: ', () => {

        it('Does not allow empty coordinates', done => {
            pointListsApi.createList('Does not allow empty coordinates')
                .then(listId => pointsApi.createPointRaw(listId, {x: null, y: 10})
                    .expect(400)
                    .then(res => {
                        expect(res.body.errors[0].field).to.equal('x');
                        expect(res.body.errors[0].message).to.not.be.undefined;
                    })
                )
                .then(listId => pointsApi.createPointRaw(listId, {x: 10, y: null})
                    .expect(400)
                    .then(res => {
                        expect(res.body.errors[0].field).to.equal('y');
                        expect(res.body.errors[0].message).to.not.be.undefined;
                    })
                )
                .then(() => done())
                .catch(err => done(err));
        });

        it('Does not allow coordinates outside [-5000 to 5000]', done => {
            pointListsApi.createList('Does not allow coordinates outside [-5000 to 5000]')
                .then(listId => pointsApi.createPointRaw(listId, {x: -5001, y: 10})
                    .expect(400)
                    .then(res => {
                        expect(res.body.errors[0].field).to.equal('x');
                        expect(res.body.errors[0].message).to.not.be.undefined;
                    })
                )
                .then(listId => pointsApi.createPointRaw(listId, {x: 5001, y: 10})
                    .expect(400)
                    .then(res => {
                        expect(res.body.errors[0].field).to.equal('x');
                        expect(res.body.errors[0].message).to.not.be.undefined;
                    })
                )
                .then(listId => pointsApi.createPointRaw(listId, {x: 10, y: -5001})
                    .expect(400)
                    .then(res => {
                        expect(res.body.errors[0].field).to.equal('y');
                        expect(res.body.errors[0].message).to.not.be.undefined;
                    })
                )
                .then(listId => pointsApi.createPointRaw(listId, {x: 10, y: 5001})
                    .expect(400)
                    .then(res => {
                        expect(res.body.errors[0].field).to.equal('y');
                        expect(res.body.errors[0].message).to.not.be.undefined;
                    })
                )
                .then(() => done())
                .catch(err => done(err));
        });

        it('Cannot add duplicate coordinates', done => {
            pointListsApi.createList('Cannot add duplicate coordinates')
                .then(listId => pointsApi.createPoint(listId, {x: 10, y: 10}))
                .then(({listId, pointId}) => pointsApi.createPointRaw(listId, {x: 10, y: 10})
                    .expect(409)
                    .then(res => {
                        expectCoordinateConflict(res);
                    })
                )
                .then(() => done())
                .catch(err => done(err));
        });

        it('Cannot update to duplicate coordinates', done => {
            pointListsApi.createList('Cannot update to duplicate coordinates')
                .then(listId => pointsApi.createPoint(listId, {x: 10, y: 10}))
                .then(({listId}) => pointsApi.createPoint(listId, {x: 11, y: 11}))
                .then(({listId, pointId}) => pointsApi.updatePoint(listId, pointId, {x: 10, y: 10})
                    .expect(409)
                    .then(res => {
                        expectCoordinateConflict(res);
                    })
                )
                .then(() => done())
                .catch(err => done(err));
        });
    });
});

function expectToContainPoint(points, listId, x, y) {
    const pointsWithoutId = points.map(({id, listId, x, y}) => {
        return {id: !!id, listId, x, y};
    });
    expect(pointsWithoutId).to.include.something.that.deep.equals({id: true, listId, x, y});
}

function expectCorrectSquareStructure(square) {
    expectPointNotToBeEmpty(square.p1);
    expectPointNotToBeEmpty(square.p2);
    expectPointNotToBeEmpty(square.p3);
    expectPointNotToBeEmpty(square.p4);
}

function expectPointNotToBeEmpty(point) {
    expect(point).to.not.be.undefined;
    expect(point.id).to.be.a('number');
    expect(point.x).to.be.a('number');
    expect(point.y).to.be.a('number');
}

var expectCoordinateConflict = function (res) {
    expect(res.body.errors[0].field).to.equal('x');
    expect(res.body.errors[1].field).to.equal('y');
    expect(res.body.errors[0].metaData.refId).to.be.not.undefined;
};
