const expect = require('chai').expect;
const httpTestUtils = require('./http-test.utils');

const POINT_LISTS_PATH = '/api/point-lists';
class PointListsApi {
    constructor(agent) {
        this.agent = agent;
    }

    retrieveLists(queryString = '') {
        return this.agent.get(`${POINT_LISTS_PATH}?${queryString}`)
            .expect(200);
    }

    retrieveList(id) {
        return this.agent.get(`${POINT_LISTS_PATH}/${id}`);
    }

    createListRaw(name) {
        return this.agent.post(POINT_LISTS_PATH).send({name})
    }

    createList(name) {
        return this.createListRaw(name)
            .expect(201)
            .then(res => {
                expect(res.header['location']).to.not.be.undefined;
                return httpTestUtils.extractIdLocation(res);
            });
    }

    updateList(id, name) {
        return this.agent.put(`${POINT_LISTS_PATH}/${id}`).send({name})
    }

    deleteList(id) {
        return this.agent.delete(`${POINT_LISTS_PATH}/${id}`)
            .expect(200)
            .then(() => id);
    }
}

class PointsApi {
    constructor(agent) {
        this.agent = agent;
    }

    createPointRaw(listId, {x, y}) {
        return this.agent.post(`${POINT_LISTS_PATH}/${listId}/points`).send({x, y});
    }

    createPoint(listId, {x, y}) {
        return this.createPointRaw(listId, {x, y})
            .expect(201)
            .then(res => {
                expect(res.header['location']).to.not.be.undefined;
                return {listId, pointId: httpTestUtils.extractIdLocation(res)};
            })
    }

    updatePoint(listId, pointId, {x, y}) {
        return this.agent.put(`${POINT_LISTS_PATH}/${listId}/points/${pointId}`).send({x, y})
    }

    retrievePoints(listId) {
        return this.agent.get(`${POINT_LISTS_PATH}/${listId}/points`)
            .expect(200);
    }

    retrievePoint(listId, pointId) {
        return this.agent.get(`${POINT_LISTS_PATH}/${listId}/points/${pointId}`);
    }

    deletePoint(listId, pointId) {
        return this.agent.delete(`${POINT_LISTS_PATH}/${listId}/points/${pointId}`)
            .expect(200)
            .then(() => {
                return {listId, pointId};
            });
    }

    retrieveSqueres(listId) {
        return this.agent.get(`${POINT_LISTS_PATH}/${listId}/points/squares`)
            .expect(200);
    }
}

module.exports.PointsApi = PointsApi;
module.exports.PointListsApi = PointListsApi;
