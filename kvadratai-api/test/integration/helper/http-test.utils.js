module.exports = {
    extractIdLocation: res => {
        let locationHeader = res.header['location'];
        let index = locationHeader.lastIndexOf('/');
        return +locationHeader.slice(index + 1);
    }
};
