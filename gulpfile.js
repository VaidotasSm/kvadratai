const gulp = require('gulp');
const jshint = require('gulp-jshint');

const runSequence = require('run-sequence');
gulp.task('build', function (callback) {
    return runSequence(
        'lint',
        'test-main',
        'test-integration',
        'clean',
        'bundle-js',
        'process-html',
        'copy-resources',
        callback
    )
});

gulp.task('lint', function () {
    return gulp.src([
        'kvadratai-api/src/**/*.js'
    ])
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(jshint.reporter('fail'));
});

const mocha = require('gulp-mocha');
gulp.task('test-main', function () {
    return gulp.src('kvadratai-api/test/main/**/*.js', {read: false})
        .pipe(mocha({reporter: 'dot'}));
});
gulp.task('test-integration', function () {
    return gulp.src('kvadratai-api/test/integration/**/*.js', {read: false})
        .pipe(mocha({reporter: 'dot'}));
});

const del = require('del');
gulp.task('clean', function () {
    return del(['kvadratai-ui/dist/**/*']);
});

const exec = require('child_process').exec;
gulp.task('bundle-js', (cb) => {
    exec('jspm bundle-sfx --minify --no-mangle src/main.js kvadratai-ui/dest/main.bundle.js', function (err, stdout, stderr) {
        cb(err);
    });
});

const htmlreplace = require('gulp-html-replace');
gulp.task('process-html', function () {
    const timeStamp = new Date().getTime();
    return gulp.src('kvadratai-ui/index.html')
        .pipe(htmlreplace({
            'js-replace': `main.bundle.js?version=${timeStamp}`,
            'css-replace': `bootstrap.min.css`
        }))
        .pipe(gulp.dest('kvadratai-ui/dest'));
});

gulp.task('copy-resources', function () {
    gulp.src('kvadratai-ui/jspm_packages/npm/bootswatch@3.3.7/flatly/bootstrap.min.css')
        .pipe(gulp.dest('kvadratai-ui/dest'));
});

const nodemon = require('gulp-nodemon');
gulp.task('start-dev', function () {
    nodemon({
        script: 'kvadratai-api/src/main.js',
        args: ['dev', '--list'],
        ext: 'js',
        ignore: ['./node_modules', 'kvadratai-ui'],
        env: {
            PROFILE: 'dev',
            PORT: 8080
        }
    }).on('restart', function () {
        console.log('Restarting…');
    });
});
